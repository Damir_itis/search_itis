import requests
from bs4 import BeautifulSoup
import os


class WebsiteCrawler:
    def __init__(self, start_url, max_pages=100):
        self.start_url = start_url
        self.max_pages = max_pages
        self.links = [start_url]
        self.visited_links = set()
        self.visited_count = 0

    def crawl_website(self):
        while self.visited_count <= self.max_pages and self.links:
            link = self.links.pop()
            if link not in self.visited_links:
                self.process_link(link)

    def process_link(self, link):
        response = requests.get(link)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.extract_links(soup)
        self.save_page(soup.encode())
        self.add_to_index(link)

    def extract_links(self, soup):
        [s.decompose() for s in soup.find_all(['iframe', 'script', "noscript", "style", "link"])]
        sub_links = soup.find_all("a", href=True)

        for sub_link in sub_links:
            href = sub_link.get("href")
            if href.startswith("/"):
                href = self.start_url + href

            if not href.startswith(self.start_url):
                continue

            if "?" in href:
                href = href.split("?")[0]

            if href[-1] == '/':
                href = href[:-1]

            if (href and href.startswith(self.start_url) and
                    href not in self.visited_links):
                self.links.append(href)

    def save_page(self, content):
        directory = "./pages"
        if not os.path.exists(directory):
            os.makedirs(directory)
        file_name = os.path.join(directory, f"page-{self.visited_count + 1}.html")
        with open(file_name, 'wb') as f:
            f.write(content)
        print(f"Страница сохранена в файле: {file_name}")
        self.visited_count += 1

    def add_to_index(self, url):
        with open("index.txt", "a") as myfile:
            myfile.write(f"{self.visited_count} {url}\n")
        print("Информация добавлена в файл index.txt")


if __name__ == "__main__":
    start_url = "https://e-katalog.com.ru"
    crawler = WebsiteCrawler(start_url)
    crawler.crawl_website()
