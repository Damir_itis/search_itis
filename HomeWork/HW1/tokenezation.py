import os
import re
import pymorphy2
from bs4 import BeautifulSoup


class HTMLProcessor:
    def __init__(self):
        self.html_files = []

    def download_htmls(self):
        for i in range(1, 102):
            with open(f"pages/page-{i}.html", 'r', encoding='utf-8') as file:
                html_content = file.read()
                self.html_files.append(html_content)

    def extract_text_from_html(self):
        texts = []
        for html in self.html_files:
            soup = BeautifulSoup(html, 'html.parser')
            text = soup.get_text(" ")
            texts.append(text)
        return texts

    def divide_into_words(self, texts):
        res = []
        for text in texts:
            words = set()
            word_pattern = re.compile(r'\b[а-яА-Я]{3,}\b')
            words.update(word_pattern.findall(text))
            res.append(words)
        return res

    def group_word_by_lemma(self, words):
        res = []
        morph = pymorphy2.MorphAnalyzer()
        for word_set in words:
            title_group_words = {}
            for word in word_set:
                lemma = morph.parse(word)[0].normal_form
                title_group_words.setdefault(lemma, []).append(word)
            res.append(title_group_words)
        return res

    def save_words_to_file(self, words):
        for index, word_set in enumerate(words, start=1):
            with open(f'tokens/token_{index}.txt', 'w', encoding='utf-8') as file:
                for word in word_set:
                    file.write(word + '\n')

    def save_lemma_words_to_file(self, title_group_words):
        for index, lemma_dict in enumerate(title_group_words, start=1):
            with open(f'lemmas/lemma_{index}.txt', 'w', encoding='utf-8') as file:
                for lemma, word_list in lemma_dict.items():
                    file.write(lemma + ': ' + ' '.join(word_list) + '\n')


if __name__ == '__main__':
    processor = HTMLProcessor()
    processor.download_htmls()
    texts = processor.extract_text_from_html()
    words = processor.divide_into_words(texts)
    title_group_words = processor.group_word_by_lemma(words)
    processor.save_words_to_file(words)
    processor.save_lemma_words_to_file(title_group_words)
